resource "aws_vpc" "eks_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
}

resource "aws_subnet" "eks_subnets" {
  vpc_id = aws_vpc.eks_vpc.id
  cidr_block = "10.0.0.0/24"
  availability_zone = "us-east-1a"
}
