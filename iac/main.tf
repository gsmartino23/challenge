module "eks_cluster" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.3.0"

  cluster_name          = var.cluster_name
  cluster_version       = "1.27"
  vpc_id                = aws_vpc.eks_vpc.id
  subnets            = aws_subnet.eks_subnets.id
  tags                  = { "Environment" = "Dev" }
  # Provider OIDC for use IRSA
}

#resource "kubectl_namespace" "dev" {
#  metadata {
#    name = "dev"
#  }
#}
#
#resource "kubectl_namespace" "stage" {
#  metadata {
#    name = "stage"
#  }
#}
