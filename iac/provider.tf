terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "5.10.0"
    }
    kubectl = {
      source = "gavinbunney/kubectl"
      version = "1.14.0"
    }
  }
}

provider "aws" {
  region = var.region
}

#provider "kubectl" {
#  host                   = var.eks_cluster_endpoint
#  cluster_ca_certificate = base64decode(var.eks_cluster_ca)
#  token                  = data.aws_eks_cluster_auth.main.token
#  load_config_file       = false
#}