variable "region" {
    type = string
    default = "us-east-1"
}

variable "cluster_name" {
    type = string
    description = "Cluster Name"
    default = "eks_challenge"
}